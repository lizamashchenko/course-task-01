const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT ? process.env.PORT : 56201;
const weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

function reverseString(str) {
    let result = "";
    for(let i = str.length - 1; i >= 0; i--) {
        result += str[i];
    }
    return result;
}
function isLeapYear(year) {
    return (year % 4 === 0) && (year % 100 !== 0 || year % 400 === 0);
}

app.post('/square', bodyParser.raw({type: 'text/plain'}),  (req, res) => {
    const num = +req.body;
    if(isNaN(num)) {
        return res.status(400).send("Invalid input");
    }
    const result = num * num;
    res.send({
        number: num,
        square: result
    });
});

app.post('/reverse', bodyParser.raw({type: 'text/plain'}), (req, res) => {
    const input = req.body.toString();
    res.send(reverseString(input));
});

app.get("/date/:year/:month/:day", (req, res) => {
    const currentDate = new Date();
    const {year, month, day} = req.params;
    const inputDate = new Date(year + "-" + month + "-" + day);
    inputDate.setHours(0, 0, 0, 0);
    currentDate.setHours(0, 0, 0, 0);
    const diffTime = Math.abs(currentDate - inputDate);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    res.send({
        'weekDay' : weekDays[inputDate.getDay()],
        'isLeapYear': isLeapYear(inputDate.getFullYear()),
        'difference': diffDays,
    })
});

    app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});
